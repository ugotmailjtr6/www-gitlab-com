---
layout: markdown_page
title: "Ecommerce Motion"
description: "Accelerate improvements in our self service ecommerce purchasing experience"
canonical_path: "/company/team/structure/working-groups/ecommerce/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | 2022-01-24 |
| Target End Date | 2022-03-18 |
| Slack           | [#wg-ecommerce-motion](https://gitlab.slack.com/archives/C03012Y7UHH) (only accessible from within the company) |
| Google Doc      | [Ecommerce Motion Working Group Agenda](https://docs.google.com/document/d/1FoUek4p2ELwuQT4IY-nQof4ft2udG2Ks_jFQiIrn7is/edit#heading=h.hhbpi9bc829) (only accessible from within the company) |

## Business Goals

Self-service and e-commerce are key levers to the success of our business (also now part of 12 Cross-Functional Initiatives). They drive customer satisfaction and adoption, deliver GTM efficiency, assist in channel development and ultimately, expand our revenue and improve our position in the market.

As the demands of the business grow in complexity and the areas that self-service touches increase, it will be crucial for us to align on one, common self-service strategy.

The goal of this group is to ultimately define the most effective way to deliver on self-service and bridge the gap between where we are today and our long-term vision and objectives.

_note: [Zuora SSOT](https://gitlab.com/groups/gitlab-org/-/epics/4664) is a prerequisite of any system changes coming out of this working group. Following through on this work enables us to scale and iterate more efficiently, even if we were to recommend leveraging a COTS solution._

## Exit Criteria & Timeline

### Path 1 (2022-02-02 > 2022-02-18) - 2 weeks
1. Defined long-term / future-state of the self-service business needs across all teams and departments in a JTBD framework
    1. Kazem Kutob
    1. Omar Fernandez
1. Define current state of self-service capabilities
    1. Omar Fernandez 
    1. Alex Martin 

### Path 2 (2022-02-18 > 2022-03-04) - 2 weeks
1. Document gaps between current self-service capabilities and ideal future state
    1. Kazem Kutob
    1. Omar Fernandez
    1. Jerome Ng
1. Document risks or blockers to delivering ideal future-state basedon our existing architecture
    1. Jerome Ng
1. Vendor asessment - how would it plug into our systems
    1. Bryan Wise

### Path 3 (2022-03-07 > 2022-03-18)
1. Recommendation on how to deliver long-term objectives, including assessment on build vs buy. 
    1. Justin Farris



## Out of Scope
1. We will not be performing a deep RFP and vendor assessment and selection as part of this exercise
1. Aligning on cross-functional values and operating principles to deliver on e-commerce is out of scope in this WG, but such work may follow in a different format in the next phase




## Roles and Responsibilities

**Contributors**

DRIs held accountable to deliver on the exit criteria. Required to attend syncronous discussions and collaborate with reviewers to collect input and feedback. Similar to Responsible and Accountable in a RACI framework. 

**Collaborators**

Stakeholders with business requirements or domain expertise who will provide input to contributors. No need for regular or syncronus attendance, will engage async with contributors as appropriate. Similar to Consulted and Informed in a RACI framework.


| Working Group Role    | Person                | Title                                           |
|-----------------------|-----------------------|-------------------------------------------------|
| Executive Sponsor | Ryan O'Nell | VP Commercial |
| Facilitator | Kazem Kutob | Director, Online Sales & Self Service |
| Contributor | Justin Farris | Sr Director, Product Monetization |
| Contributor | Alex Martin | Sr Manager, Online Sales |
| Contributor | Bryan Wise | VP, IT |
| Contributor | Jerome Z Ng | Sr Engineering Manager, Fulfillment |
| Contributor | Omar Fernandez | Interim Director of Product, Fulfillment | 
| Contributor | Jerome Ng | Sr Engineering Manager, Fulfillment |
| Collaborator | Tatyana Golubeva | Principal PM, Purchase |
| Collaborator | Emily Sybrant | Product Designer, Purchase |
| Collaborator | James Lopez | Backend Engineering Manager, Fulfillment:License |
| Collaborator | Tyler Amos | Staff Backend Engineer, Fulfillment:License |
| Collaborator | Hila Qu | Director of Product, Growth |
| Collaborator | Christopher Nelson | Sr Director, Enterprise Applications |
| Collaborator | Mark Quitevis | Sr Business Systems Analyst |
| Collaborator | Jessica Salcido | Finance Systems Administrator |
| Collaborator | Daniel Parker | Senior Integrations Engineer, Business Technology |
| Collaborator | Sarah McCauley | Finance |
| Collaborator | Michelle Hodges | VP, Channel Sales |
| Collaborator | Jack Brennan | Sr Director, Sales Systems |
| Collaborator | David Duncan | VP Marketing |
| Collaborator | Michael Preuss | Senior Manager, Digital Experience |
| Collaborator | Sindhu Tatimatla | Director, Analytics & Insights |
| Collaborator | Cheri Holmes | Chief of Staff to CRO |
| Collaborator | Jake Bielecki | Sr Director, Sales Strategy and Ops |
